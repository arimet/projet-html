# README #

### Membres du groupe ###

* Anthony Rimet
* David Cordier

### Qu'est le but de ce projet ? ###

* Dans le cadre d'un projet de la LP-CISIIE de l'IUT Charlemagne de Nancy. 

* Utilise un framework que nous avons crée https://bitbucket.org/arimet/mini-framework

* Ce projet est un site de petite annonces uniquement en HTML5.

### Présentation des fonctionnalités présents ###

4 pages ont été crée.

* Page d'accueil
* Page de recherche (utilisez la barre de navigation pour s'y rendre)
* Page d'ajout d'annonce (utilisez la barre de navigation pour s'y rendre)
* Page d'une annonce (se rendre sur la page recherche et cliquez sur l'annonce en bas)


### Voir le projet ###



* Page accueil : https://webetu.iutnc.univ-lorraine.fr/www/rimet1u/Projet-html/
* Page de recherche : https://webetu.iutnc.univ-lorraine.fr/www/rimet1u/Projet-html/recherche.html
* Page d'ajout : https://webetu.iutnc.univ-lorraine.fr/www/rimet1u/Projet-html/ajout.html
* Page d'annonce : https://webetu.iutnc.univ-lorraine.fr/www/rimet1u/Projet-html/annonce.html